<?

$stamp = array(
    array(2, 4),
    array(3, 5),
    array(4, 6),
    array(4, 7),
    array(8, 20),
    array(17, 19),
    array(27, 37)
);


$ovarlap = (new Overlap)->searchOverlap($stamp);

echo '<pre>';
var_dump($ovarlap);
echo '</pre>';

class Overlap {

    public $data;
    protected $start = array();
    protected $end= array();
    protected $return= array();

    function searchOverlap($data) {

        
        if (!is_array($data) && !$data instanceof Traversable) {
            return false;
        }

        $this->data = $data;

        foreach ($data as list($from, $to)) {
            

            $period = new Period((int) $from, (int) $to);

            //add first period
            if (empty($this->start)) {
                $this->start[] = $from;
                //$duration[] = $to - $from;
                $this->end[] = $to;

                continue;
            }

            //search for crossing
            foreach ($this->start as $period_id => $period_start) {
                
                $period_end = $this->end[$period_id];
                
                if ($period->cross($period_start, $period_end) === true) {

                    $this->start[$period_id] = $period::Smallest($period_start, $from);
                    $this->end[$period_id] = $period::Biggest($period_end, $to);
                    $period->setInside(true);
                } else {

                    $period->InsideSearch($period_start, $period_end);
                }
            }

            // if no crossing with any period - add new.
            if ($period->getInside() === false) {

                $this->start[] = $from;
                //$duration[] = $to - $from;
                $this->end[] = $to;
            }
        }

        for ($i = 0; $i < count($this->start); $i++) {
            $this->return[] = array($this->start[$i], $this->end[$i]);
        }

        return $this->return;
    }

}

class Period {

    private $from;
    private $to;
    private $duration; //just in case
    private $inside = false; // flag that we don't need to add a period

    public function __construct($from, $to) {
        $this->from = $from;
        $this->to = $to;
        $this->duration = $to - $from;
    }

    // if period fully inside
    public function InsideSearch($a, $b) {
        if ($this->from < $a && $this->to < $b) {
            $this->inside = true;
        }

        return;
    }

    // search crossing
    public function cross($start, $end) {

        if (( $this->from > $start && $this->from < $end ) ||
                ( $this->to > $start && $this->to < $end )
        ) {
            return true;
        }

        return false;
    }

    public static function Biggest($a, $b) {
        return $a > $b ? $a : $b;
    }

    public static function Smallest($a, $b) {
        return $a > $b ? $b : $a;
    }

    public function getInside() {

        return $this->inside;
    }

    public function setInside($inside) {

        if ($inside === true) {
            $this->inside = $inside;
        } else {
            $this->inside = false;
        }

        return;
    }

}
